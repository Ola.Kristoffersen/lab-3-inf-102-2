package INF102.lab3.numberGuesser;


public class MyGeniusGuesser implements IGuesser {

	@Override
    public int findNumber(RandomNumber number) {

        int lower = number.getLowerbound();
        int upper = number.getUpperbound();

        while (lower<=upper){

            int guess = lower + (upper - lower) / 2 ;
            int answer = number.guess(guess);

            if (answer == 0 ){
                return guess;
            }
            else if (answer == -1) {
                lower = guess + 1 ;
            }
            else if (answer == 1) {
                upper = guess- 1;
            }
        }
        return 0;
    }

}
