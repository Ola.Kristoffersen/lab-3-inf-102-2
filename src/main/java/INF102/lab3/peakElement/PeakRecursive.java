package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {
       
       // Finne peak element
       //Ta 1 og sjekke om den er større enn venstre og høyre
       //size - 1 = nest siste. sjekk med før og etter
       // Fjern siste

        if( numbers.isEmpty()){
            return 0;
        }

        int n = numbers.size();
        int a = numbers.get(n-3);
        int b = numbers.get(n-2);
        int c = numbers.get(n-1);

        if ( n == 1 ) {
            return numbers.get(0);

        }

        if (numbers.get(0)> numbers.get(1)){
            return numbers.get(0);

        }

        if(numbers.get(n-1) > numbers.get(n-2)){
            return numbers.get(n-1);
        }
        if (b > a && b > c) {
            return b;
        }

        numbers.remove(n-1);
        return peakElement(numbers);
        
        
    }

}
